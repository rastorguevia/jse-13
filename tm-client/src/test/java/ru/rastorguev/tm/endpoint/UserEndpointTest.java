package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.Exception;

import static org.junit.Assert.*;

public class UserEndpointTest {

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private static String token;

    @NotNull
    private static final String failToken = "12345";

    @NotNull
    private static final String login = "testUser";

    @NotNull
    private static final String password = "testPassword";

    @BeforeClass
    public static void createUser() throws Exception_Exception {
        userEndpoint.createUser(login, password);
        token = sessionEndpoint.createNewSession(login, password);
    }

    @Test(expected = Exception.class)
    public void findCurrentUserFail() throws Exception_Exception {
        userEndpoint.findCurrentUser(failToken);
    }

    @Test
    public void findCurrentUser() throws Exception_Exception {
       @Nullable final User user = userEndpoint.findCurrentUser(token);
       assertNotNull(user);
    }

    @Test
    public void editUserLogin() throws Exception_Exception {
        @NotNull final String testLogin = "testUser1";
        userEndpoint.updateUserLogin(token, testLogin);
        @Nullable final User user = userEndpoint.findCurrentUser(token);
        if (user == null) throw new Exception_Exception();
        assertEquals(testLogin, user.getLogin());
    }

    @Test(expected = Exception.class)
    public void editUserPasswordFail() throws Exception_Exception {
        @NotNull final String updatePassword = "newpassword";
        @NotNull final String oldPassword = "12345";
        userEndpoint.updateUserPassword(token, updatePassword, oldPassword);
    }

    @Test(expected = Exception.class)
    public void editUserPasswordFailToken() throws Exception_Exception {
        @NotNull final String updatePassword = "newpassword";
        @NotNull final String oldPassword = "12345";
        userEndpoint.updateUserPassword(failToken, updatePassword, password);
    }

    @Test(expected = Exception.class)
    public void removeUser() throws Exception_Exception {
        @NotNull final String login = "removeUser";
        @NotNull final String password = "removeUserPassword";
        userEndpoint.createUser(login, password);

        @Nullable final String userToken = sessionEndpoint.createNewSession(login, password);
        userEndpoint.removeUser(userToken);
        userEndpoint.findCurrentUser(userToken);
    }

    @Test(expected = Exception.class)
    public void createAdminFail() throws Exception_Exception {
        @NotNull final String adminLogin = "removeUser";
        @NotNull final String adminPassword = "removeUserPassword";
        userEndpoint.createAdmin(token, adminLogin, adminPassword);
    }

    @Test
    public void createAdmin() throws Exception_Exception {
        //def admin
        @NotNull final String adminLogin = "admin";
        @NotNull final String adminPassword = "qwerty";
        @Nullable final String adminToken = sessionEndpoint.createNewSession(adminLogin, adminPassword);

        @NotNull final String createAdminLogin = "adminTest";
        @NotNull final String createAdminPassword = "adminTest";
        userEndpoint.createAdmin(adminToken, createAdminLogin, createAdminPassword);

        @Nullable final String createdAdminToken = sessionEndpoint.createNewSession(createAdminLogin, createAdminPassword);
        @Nullable final User createdAdmin = userEndpoint.findCurrentUser(createdAdminToken);
        assertNotNull(createdAdmin);
    }

    @AfterClass
    public static void clear() throws Exception_Exception {
        userEndpoint.removeUser(token);
        @Nullable final String createdAdminToken = sessionEndpoint.createNewSession("adminTest", "adminTest");
        userEndpoint.removeUser(createdAdminToken);
    }

}
