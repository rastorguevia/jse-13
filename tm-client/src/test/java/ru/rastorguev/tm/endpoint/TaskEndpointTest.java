package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import java.lang.Exception;
import java.util.UUID;

public class TaskEndpointTest {

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private static final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final Project project1 = new Project();

    @NotNull
    private static final Project project2 = new Project();

    @Nullable
    private static String token;

    @Nullable
    private static String userId;

    @BeforeClass
    public static void createUserAndProjects() throws Exception_Exception {
        @NotNull final String login = "testUser";
        @NotNull final String password = "testPassword";
        userEndpoint.createUser(login, password);
        token = sessionEndpoint.createNewSession(login, password);
        userId = userEndpoint.getUserId(token);

        project1.setId(UUID.randomUUID().toString());
        project1.setUserId(userId);
        project1.setName("project 1 user: testUser");
        project1.setDescription("project description");
        projectEndpoint.createProject(token, project1);

        project2.setId(UUID.randomUUID().toString());
        project2.setUserId(userId);
        project2.setName("project 2 user: testUser");
        project2.setDescription("project description");
        projectEndpoint.createProject(token, project2);
    }

    @Test
    public void createTask() throws Exception_Exception {
        final int taskCount1 = taskEndpoint.getTaskListByProjectId(token, project1.getId()).size();
        final int taskCount2 = taskEndpoint.getTaskListByProjectId(token, project2.getId()).size();

        taskEndpoint.createTask(token, getNewTask(project1));
        taskEndpoint.createTask(token, getNewTask(project1));
        taskEndpoint.createTask(token, getNewTask(project1));
        taskEndpoint.createTask(token, getNewTask(project2));
        taskEndpoint.createTask(token, getNewTask(project2));

        assertEquals(taskCount1 + 3, taskEndpoint.getTaskListByProjectId(token, project1.getId()).size());
        assertEquals(taskCount2 + 2, taskEndpoint.getTaskListByProjectId(token, project2.getId()).size());
    }

    @Test(expected = Exception.class)
    public void createTaskFail() throws Exception_Exception {
        taskEndpoint.createTask("token", getNewTask(project1));
    }

    @Test
    public void editTask() throws Exception_Exception {
        @NotNull final Task task = taskEndpoint.createTask(token, getNewTask(project1));
        @NotNull final String name = "task name edited";
        @NotNull final String description = "task description edited";

        task.setName(name);
        task.setDescription(description);
        @NotNull final Task editedTask = taskEndpoint.updateTask(token, task);
        assertEquals(name, editedTask.getName());
        assertEquals(description, editedTask.getDescription());
    }

    @Test
    public void removeTask() throws Exception_Exception {
        final int taskCount = taskEndpoint.getTaskListByProjectId(token, project1.getId()).size();

        @NotNull final Task task1 = getNewTask(project1);
        @NotNull final Task task2 = getNewTask(project1);

        taskEndpoint.createTask(token, task1);
        taskEndpoint.createTask(token, task2);
        assertEquals(taskCount + 2, taskEndpoint.getTaskListByProjectId(token, project1.getId()).size());

        taskEndpoint.removeTask(token, task1.getId());

        assertEquals(taskCount + 1, taskEndpoint.getTaskListByProjectId(token, project1.getId()).size());

        taskEndpoint.removeTask(token, task2.getId());
        assertEquals(taskCount, taskEndpoint.getTaskListByProjectId(token, project1.getId()).size());
    }

    @Test
    public void removeAllProjectTasks() throws Exception_Exception {
        taskEndpoint.createTask(token, getNewTask(project1));
        taskEndpoint.createTask(token, getNewTask(project1));
        taskEndpoint.createTask(token, getNewTask(project1));

        final int taskCount = taskEndpoint.getTaskListByProjectId(token, project1.getId()).size();

        taskEndpoint.removeTaskListByProjectId(token, project1.getId());

        assertEquals(0, taskEndpoint.getTaskListByProjectId(token, project1.getId()).size());
    }

    @Test
    public void findTaskByNameOrDescription() throws Exception_Exception {
        @NotNull final Task task1 = getNewTask(project1);
        @NotNull final Task task2 = getNewTask(project1);

        task1.setName("key");
        task2.setDescription("key123");

        @NotNull final Task task3 = getNewTask(project2);
        @NotNull final Task task4 = getNewTask(project2);

        task3.setName("123key");
        task4.setDescription("car");

        taskEndpoint.createTask(token, task1);
        taskEndpoint.createTask(token, task2);
        taskEndpoint.createTask(token, task3);
        taskEndpoint.createTask(token, task4);

        assertEquals(3, taskEndpoint.filteredTaskListByInput(token, "key").size());
    }

    @AfterClass
    public static void clear() throws Exception_Exception {
        taskEndpoint.removeTaskListByProjectId(token, project1.getId());
        taskEndpoint.removeTaskListByProjectId(token, project2.getId());
        projectEndpoint.removeAllProjectsByUser(token);
        userEndpoint.removeUser(token);
    }

    @NotNull
    static Task getNewTask(@NotNull final Project project) {
       @NotNull final Task task = new Task();
       task.setId(UUID.randomUUID().toString());
       task.setUserId(userId);
       task.setName("task name user: testUser");
       task.setDescription("task description user: testUser");
       task.setProjectId(project.getId());
       task.setProjectName(project.getName());
       return task;
    }

}
