package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.lang.Exception;
import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectEndpointTest {

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private static String token;

    @Nullable
    private static String userId;

    @BeforeClass
    public static void createUser() throws Exception_Exception {
        @NotNull final String login = "testUser";
        @NotNull final String password = "testPassword";
        userEndpoint.createUser(login, password);
        token = sessionEndpoint.createNewSession(login, password);
        userId = userEndpoint.getUserId(token);
    }

    @Test
    public void createProject() throws Exception_Exception {
        final int projectCount = projectEndpoint.findAllProjectsForUser(token).size();
        @NotNull final Project project1 = getNewProject();
        @NotNull final Project project2 = getNewProject();
        @NotNull final Project project3 = getNewProject();
        projectEndpoint.createProject(token, project1);
        assertEquals(projectCount + 1, projectEndpoint.findAllProjectsForUser(token).size());
        projectEndpoint.createProject(token, project2);
        assertEquals(projectCount + 2, projectEndpoint.findAllProjectsForUser(token).size());
        projectEndpoint.createProject(token, project3);
        assertEquals(projectCount + 3, projectEndpoint.findAllProjectsForUser(token).size());
    }

    @Test(expected = Exception.class)
    public void createProjectFail() throws Exception_Exception {
        projectEndpoint.createProject("token", getNewProject());
    }

    @Test
    public void editProject() throws Exception_Exception {
        @NotNull final Project project = getNewProject();
        projectEndpoint.createProject(token, project);
        @NotNull final String name = "edited project name";
        @NotNull final String description = "edited project description";
        project.setName(name);
        project.setDescription(description);
        projectEndpoint.updateProject(token, project);

        @NotNull final String editedProjectName = projectEndpoint.findProject(token, project.getId()).getName();
        @NotNull final String editedProjectDescription = projectEndpoint.findProject(token, project.getId()).getDescription();

        assertEquals(editedProjectName, name);
        assertEquals(editedProjectDescription, description);
    }

    @Test
    public void editProjectFail() throws Exception_Exception {
        @NotNull final Project project = getNewProject();
        projectEndpoint.createProject(token, project);
        project.setUserId("qwerty12345");
        project.setName("edited project name");
        projectEndpoint.updateProject(token, project);
        @NotNull final Project editedProject = projectEndpoint.findProject(token, project.getId());
        assertNotEquals(project.getName(), editedProject.getName());
    }

    @Test
    public void removeProject() throws Exception_Exception {
        final int projectCount = projectEndpoint.findAllProjectsForUser(token).size();
        @NotNull final Project project1 = getNewProject();
        @NotNull final Project project2 = getNewProject();

        projectEndpoint.createProject(token, project1);
        projectEndpoint.createProject(token, project2);
        assertEquals(projectCount + 2, projectEndpoint.findAllProjectsForUser(token).size());
        projectEndpoint.removeProject(token, project1.getId());
        assertEquals(projectCount + 1, projectEndpoint.findAllProjectsForUser(token).size());
    }

    @Test
    public void projectList() throws Exception_Exception {
        final int projectCount = projectEndpoint.findAllProjectsForUser(token).size();
        projectEndpoint.createProject(token, getNewProject());
        projectEndpoint.createProject(token, getNewProject());
        projectEndpoint.createProject(token, getNewProject());
        assertEquals(projectCount + 3, projectEndpoint.findAllProjectsForUser(token).size());
    }

    @Test
    public void findProjectByNameOrDescription() throws Exception_Exception {
        @NotNull final Project project1 = getNewProject();
        @NotNull final Project project2 = getNewProject();
        @NotNull final Project project3 = getNewProject();
        project1.setName("key");
        project2.setDescription("key123");
        project3.setName("123key");
        @NotNull final Project project4 = getNewProject();
        project4.setName("value");
        project4.setDescription("value");

        projectEndpoint.createProject(token, project1);
        projectEndpoint.createProject(token, project2);
        projectEndpoint.createProject(token, project3);
        projectEndpoint.createProject(token, project4);

        assertEquals(3, projectEndpoint.findProjectsByUserIdAndInput(token, "key").size());
    }

    @AfterClass
    public static void clear() throws Exception_Exception {
        projectEndpoint.removeAllProjectsByUser(token);
        userEndpoint.removeUser(token);
    }

    @NotNull
    static Project getNewProject() {
        @NotNull final Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(userId);
        project.setStatus(Status.IN_PROGRESS);
        project.setName("project user: testUser");
        project.setDescription("project description");
        return project;
    }

}
