package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.endpoint.Role;

public interface IStateService {

    String getToken();

    void setToken(String token);

    Role getUserRole();

    void setUserRole(Role userRole);

    boolean isAuth();

    boolean isRolesAllowed(final Role... roles);

}
