package ru.rastorguev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.error.FailException;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "project_create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Project create");

        @Nullable final String token = serviceLocator.getStateService().getToken();
        if (token == null || token.isEmpty()) throw new FailException("Log in to open new session.");

        @Nullable  final String userId = serviceLocator.getUserEndpoint().getUserId(token);
        if (userId == null || userId.isEmpty()) throw new FailException("Session is not valid.");

        System.out.println("Enter name");
        @NotNull final String inputName = serviceLocator.getTerminalService().nextLine();
        if (inputName.isEmpty()) throw new FailException("Empty name. Try again.");

        System.out.println("Enter description");
        @NotNull final String inputDescription = serviceLocator.getTerminalService().nextLine();
        if (inputDescription.isEmpty()) throw new FailException("Empty description. Try again.");

        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(inputName);
        project.setDescription(inputDescription);

        serviceLocator.getProjectEndpoint().createProject(token, project);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }

}