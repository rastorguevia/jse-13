package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Task;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.util.NumberUtil.isInt;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task remove");

        @Nullable final String token = serviceLocator.getStateService().getToken();
        if (token == null || token.isEmpty()) throw new FailException("Log in to open new session.");

        System.out.println("Enter Project ID");
        @NotNull final List<Project> projectList =  serviceLocator.getProjectEndpoint().findAllProjectsForUser(token);
        printAllProjectsForUser(projectList);
        @NotNull final String inputProjectNumber = serviceLocator.getTerminalService().nextLine();
        final int projectNumber = isInt(inputProjectNumber);
        if (projectNumber == 0) throw new FailException("Entered value is not a number.");
        @Nullable final String projectId = serviceLocator.getProjectEndpoint().projectIdByNumber(token, projectNumber);
        if (projectId == null || projectId.isEmpty()) throw new FailException("Project could not be found.");
        @NotNull final List<Task> taskListByProjectId = serviceLocator.getTaskEndpoint().getTaskListByProjectId(token, projectId);
        printTaskListByProjectId(taskListByProjectId);

        System.out.println("Enter Task ID");
        @NotNull final String inputTaskNumber = serviceLocator.getTerminalService().nextLine();
        final int taskNumber = isInt(inputTaskNumber);
        if (taskNumber == 0) throw new FailException("Entered value is not a number.");
        @Nullable final String taskId = serviceLocator.getTaskEndpoint().taskIdByNumberAndProjectId(token, taskNumber, projectId);
        if (taskId == null || taskId.isEmpty()) throw new FailException("Task could not be found.");
        serviceLocator.getTaskEndpoint().removeTask(token, taskId);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }

}