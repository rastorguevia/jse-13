package ru.rastorguev.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;

import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.error.FailException;

@NoArgsConstructor
public class DataSaveFasterXmlJsonCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_save_fasterxml_json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save repositories FasterXML JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data save FasterXML JSON");

        @Nullable final String token = serviceLocator.getStateService().getToken();
        if (token == null || token.isEmpty()) throw new FailException("Log in to open new session.");

        serviceLocator.getDataEndpoint().dataSaveFasterXmlJson(token);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
