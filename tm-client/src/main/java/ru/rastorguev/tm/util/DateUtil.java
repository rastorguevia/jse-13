package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {

    @NotNull
    public final static SimpleDateFormat dateFormatter = new SimpleDateFormat("d.MM.y");

    @NotNull
    public static Date stringToDate(@NotNull final String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    public static XMLGregorianCalendar stringToXmlGregorianCalendar(@NotNull final String typedDate) throws Exception {
        @Nullable final Date date = stringToDate(typedDate);
        if (date == null) throw new Exception("Null date.");
        @Nullable DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException dce) {
            throw new RuntimeException("Error during date parse.");
        }
        @NotNull final GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(date.getTime());
        return df.newXMLGregorianCalendar(gc);
    }

    @NotNull
    public static String dateToString(@Nullable final XMLGregorianCalendar date) throws Exception {
        if (date == null) throw new Exception("Null date.");
        @Nullable final String stringDate = dateFormatter.format(date.toGregorianCalendar().getTime());
        return stringDate;
    }

}