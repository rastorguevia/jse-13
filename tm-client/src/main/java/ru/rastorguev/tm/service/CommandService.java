package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.service.ICommandService;
import ru.rastorguev.tm.command.AbstractCommand;

import java.util.Map;

@RequiredArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private final Map<String, AbstractCommand> commandMap;

    @NotNull
    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }

}
