package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAllByUserId(final String userId) throws Exception;

    List<Project> findAllByUserIdSorted(final String userId, final String sortType) throws Exception;

    void removeAllByUserId(final String userId) throws Exception;

    String getProjectIdByNumberForUser(final int number, final String userId) throws Exception;

    List<Project> findProjectsByInputAndUserId(final String input, final String userId) throws Exception;

    void loadFromDto(final List<Project> projectList) throws Exception;

}
