package ru.rastorguev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Task;

import java.util.List;

public interface TaskMapper {

    @NotNull
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_table")
    List<Task> findAll() throws Exception;

    @Nullable
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_table WHERE id = #{param1}")
    Task findOne(String taskId) throws Exception;

    @Insert("INSERT INTO task_table (id, user_id, project_id, projectName, name, " +
            "description, startDate, endDate, status, creationDate) " +
            "VALUES (#{id}, #{userId}, #{projectId}, #{projectName}, #{name}, " +
            "#{description}, #{startDate}, #{endDate}, #{status}, #{creationDate})")
    void persist(Task task) throws Exception;

    @Update("UPDATE task_table SET projectName = #{projectName}, name = #{name}, description = #{description}, " +
            "startDate = #{startDate}, endDate = #{endDate}, status = #{status}, creationDate = #{creationDate} " +
            "WHERE id = #{id} AND user_id = #{userId} AND project_id = #{projectId}")
    void merge(Task task) throws Exception;

    @Delete("DELETE FROM task_table WHERE id = #{param1}")
    void remove(String taskId) throws Exception;

    @Delete("TRUNCATE task_table")
    void removeAll() throws Exception;

    @Delete("DELETE FROM task_table WHERE project_id = #{param1}")
    void removeTaskListByProjectId(final String projectId) throws Exception;

    @NotNull
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_table WHERE user_id = #{param1}")
    List<Task> taskListByUserId(final String userId) throws Exception;

    @NotNull
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_table WHERE user_id = #{param1} " +
            "AND name LIKE '%' #{param2} '%' OR description LIKE '%' #{param2} '%'")
    List<Task> filteredTaskListByUserIdAndInput(final String userId, final String input) throws Exception;

    @NotNull
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_table WHERE project_id = #{param1} ORDER BY creationDate ASC")
    List<Task> taskListByProjectId(final String projectId) throws Exception;

    @NotNull
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_table WHERE project_id = #{param1} ORDER BY #{param2}")
    List<Task> taskListByProjectIdSorted(final String projectId, final String sortType) throws Exception;

}
