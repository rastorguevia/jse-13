package ru.rastorguev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.User;

import java.util.List;

public interface UserMapper {

    @NotNull
    @Select("SELECT * FROM user_table")
    List<User> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM user_table WHERE id = #{param1}")
    User findOne(String userId) throws Exception;

    @Insert("INSERT INTO user_table (id, login, passHash, role) " +
            "VALUES(#{id}, #{login}, #{passHash}, #{role})")
    void persist(User user) throws Exception;

    @Update("UPDATE user_table SET login = #{login}, passHash = #{passHash}, role = #{role} WHERE id = #{id}")
    void merge(User user) throws Exception;

    @Delete("DELETE FROM user_table WHERE id = #{param1}")
    void remove(String userId) throws Exception;

    @Delete("TRUNCATE user_table")
    void removeAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM user_table WHERE login = #{param1}")
    User findByLogin(final String login) throws Exception;

    @Select("SELECT EXISTS(SELECT * FROM user_table WHERE login = #{param1})")
    boolean isExistByLogin(final String login) throws Exception;

}
