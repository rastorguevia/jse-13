package ru.rastorguev.tm.api.entity;

import ru.rastorguev.tm.enumerated.Status;

import java.util.Date;

public interface ComparableEntity {

    Date getStartDate();

    Date getEndDate();

    Status getStatus();

    Long getCreationDate();

}
