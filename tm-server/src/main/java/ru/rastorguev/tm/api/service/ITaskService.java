package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void removeTaskListByProjectId(final String projectId) throws Exception;

    String getTaskIdByNumberAndProjectId(final int number, String projectId) throws Exception;

    List<Task> taskListByUserId(final String userId) throws Exception;

    List<Task> taskListByProjectIdSorted(final String projectId, final String sortType) throws Exception;

    List<Task> filteredTaskListByUserIdAndInput(final String userId, final String input) throws Exception;

    List<Task> taskListByProjectId(final String projectId) throws Exception;

    void loadFromDto(final List<Task> taskList) throws Exception;

}
