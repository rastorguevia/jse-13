package ru.rastorguev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Session;

import java.util.List;

public interface SessionMapper {

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM session_table")
    List<Session> findAll() throws Exception;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM session_table WHERE id = #{param1}")
    Session findOne(String sessionId) throws Exception;

    @Insert("INSERT INTO session_table (id, user_id, role, timestamp, signature) " +
            "VALUES(#{id}, #{userId}, #{role}, #{timestamp}, #{signature})")
    void persist(Session session) throws Exception;

    @Update("UPDATE session_table SET role = #{role}, timestamp = #{timestamp}, " +
            "signature = #{signature} WHERE id = #{id} AND user_id = #{userId}")
    void merge(Session session) throws Exception;

    @Delete("DELETE FROM session_table WHERE id = #{param1}")
    void remove(String sessionId) throws Exception;

    @Delete("TRUNCATE session_table")
    void removeAll() throws Exception;

}
