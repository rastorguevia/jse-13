package ru.rastorguev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Project;

import java.util.List;

public interface ProjectMapper {

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM project_table")
    List<Project> findAll() throws Exception;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM project_table WHERE id = #{param1}")
    Project findOne(@NotNull String projectId) throws Exception;

    @Insert("INSERT INTO project_table (id, user_id, name, description, startDate, endDate, status, creationDate) " +
            "VALUES (#{id}, #{userId}, #{name}, #{description}, #{startDate}, #{endDate}, #{status}, #{creationDate})")
    void persist(Project project) throws Exception;

    @Update("UPDATE project_table SET name = #{name}, description = #{description}, " +
            "startDate = #{startDate}, endDate = #{endDate}, status = #{status}, " +
            "creationDate = #{creationDate} WHERE id = #{id} AND user_id = #{userId}")
    void merge(Project project) throws Exception;

    @Delete("DELETE FROM project_table WHERE id = #{param1}")
    void remove(String projectId) throws Exception;

    @Delete("TRUNCATE project_table")
    void removeAll() throws Exception;

    @Delete("DELETE FROM project_table WHERE user_id = #{param1}")
    void removeAllByUserId(final String userId) throws Exception;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM project_table WHERE user_id = #{param1} ORDER BY creationDate ASC")
    List<Project> findAllByUserId(final String userId) throws Exception;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM project_table WHERE user_id = #{param1} ORDER BY #{param2}")
    List<Project> findAllByUserIdSorted(final String userId, final String sortType) throws Exception;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM project_table WHERE user_id = #{param2} AND (name LIKE '%' #{param1} '%' OR description LIKE '%' #{param1} '%')")
    List<Project> findProjectsByInputAndUserId(final String input, final String userId) throws Exception;

}
