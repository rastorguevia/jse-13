package ru.rastorguev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum Role {

    ADMINISTRATOR ("Administrator"),
    USER ("User");

    @Getter
    @NotNull
    private final String displayName;

}