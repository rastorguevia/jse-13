package ru.rastorguev.tm.util;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ArrayUtil {

    public static boolean isIndexExist(@Nullable final List<?> checkList, final int index) {
        if (checkList == null)
            return false;
        return index >= 0 && index < checkList.size();
    }

}
