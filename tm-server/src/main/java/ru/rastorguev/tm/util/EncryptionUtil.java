package ru.rastorguev.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.entity.Session;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public final class EncryptionUtil {

    @NotNull
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static SecretKeySpec secretKey;

    public static void setKey(@Nullable final String myKey) throws Exception {
        if (myKey == null || myKey.isEmpty()) throw new Exception("Key is empty.");
        try {
            MessageDigest sha = null;
            byte[] key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (Exception e) {
            throw new Exception("Key generation error." + e);
        }
    }

    @Nullable
    public static String encrypt(@Nullable final Session sessionToEncrypt) throws Exception {
        if (sessionToEncrypt == null) throw new Exception("Session to encrypt is empty.");
        @NotNull final String sessionJson = objectMapper.writeValueAsString(sessionToEncrypt);
        @NotNull final String secret = PropertyUtil.getServerProperty(Constant.SERVER_SECRET);
        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        final byte[] sessionData = cipher.doFinal(sessionJson.getBytes("UTF-8"));
        @NotNull final String token = Base64.getEncoder().encodeToString(sessionData);
        return token;
    }

    @Nullable
    public static Session decrypt(@Nullable final String strToDecrypt) throws Exception {
        if (strToDecrypt == null || strToDecrypt.isEmpty()) throw new Exception("String to decrypt is empty.");
        @NotNull final String secret = PropertyUtil.getServerProperty(Constant.SERVER_SECRET);
        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        final byte[] sessionData = Base64.getDecoder().decode(strToDecrypt);
        @NotNull final String sessionJson = new String(cipher.doFinal(sessionData));
        @NotNull final Session session = objectMapper.readValue(sessionJson, Session.class);
        return session;
    }

}
