package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.mapper.TaskMapper;
import ru.rastorguev.tm.api.service.ISessionFactoryService;
import ru.rastorguev.tm.api.service.ITaskService;
import ru.rastorguev.tm.entity.Task;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ISessionFactoryService sessionFactoryService;

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> taskList = mapper.findAll();
        sqlSession.close();
        return taskList;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String taskId) throws Exception {
        if (taskId == null || taskId.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @Nullable final Task task = mapper.findOne(taskId);
        sqlSession.close();
        return task;
    }

    @Nullable
    @Override
    public Task persist(@Nullable final Task task) throws Exception {
        if (task == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try{
            mapper.persist(task);
            sqlSession.commit();
            return task;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task merge(@Nullable final Task task) throws Exception {
        if (task == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try{
            mapper.merge(task);
            sqlSession.commit();
            return task;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String taskId) throws Exception {
        if (taskId == null || taskId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try{
            mapper.remove(taskId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeTaskListByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try{
            mapper.removeTaskListByProjectId(projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> taskList = mapper.taskListByProjectId(projectId);
        sqlSession.close();
        return taskList;
    }

    @Nullable
    @Override
    public String getTaskIdByNumberAndProjectId(final int number, @Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull List<Task> filteredListOfTasks = taskListByProjectId(projectId);
        if (filteredListOfTasks == null) return null;
        return filteredListOfTasks.get(number - 1).getId();
    }

    @NotNull
    @Override
    public List<Task> taskListByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> taskList = mapper.taskListByUserId(userId);
        sqlSession.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> filteredTaskListByUserIdAndInput(@NotNull final String userId, @NotNull final String input) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (input == null || input.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> taskList = mapper.filteredTaskListByUserIdAndInput(userId, input);
        sqlSession.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectIdSorted(@Nullable final String projectId, @Nullable final String sortType) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> taskList = mapper.taskListByProjectIdSorted(projectId, sortType);
        sqlSession.close();
        return taskList;
    }

    @Override
    public void loadFromDto(@Nullable final List<Task> taskList) throws Exception {
        if (taskList == null) return;
        for (final Task task : taskList){
            @Nullable Task t = findOne(task.getId());
            if (t == null) persist(task);
            else merge(task);
        }
    }

}