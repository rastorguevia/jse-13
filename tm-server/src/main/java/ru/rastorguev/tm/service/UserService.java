package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.mapper.UserMapper;
import ru.rastorguev.tm.api.service.ISessionFactoryService;
import ru.rastorguev.tm.api.service.IUserService;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

import java.util.List;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

@RequiredArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final ISessionFactoryService sessionFactoryService;

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        @NotNull final List<User> userList = mapper.findAll();
        sqlSession.close();
        return userList;
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        @Nullable final User user = mapper.findOne(userId);
        sqlSession.close();
        return user;
    }


    @Override
    public User persist(@Nullable final User user) throws Exception {
        if (user == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            mapper.persist(user);
            sqlSession.commit();
            return user;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) throws Exception {
        if (user == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            mapper.merge(user);
            sqlSession.commit();
            return user;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            mapper.remove(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        @Nullable final User user = mapper.findByLogin(login);
        sqlSession.close();
        return user;
    }

    @Override
    public boolean isExistByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        boolean isExist = mapper.isExistByLogin(login);
        sqlSession.close();
        return isExist;
    }

    @Override
    public User createUser(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassHash(mdHashCode(password));
        persist(user);
        return user;
    }

    @Override
    public User createAdmin(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final User admin = new User();
        admin.setLogin(login);
        admin.setPassHash(mdHashCode(password));
        admin.setRole(Role.ADMINISTRATOR);
        persist(admin);
        return admin;
    }

    @Override
    public void updateUserLogin(@Nullable final String userId, @Nullable final String login) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Login is empty");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("Login already exist.");
        @Nullable final User user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        user.setLogin(login);
        merge(user);
    }

    @Override
    public void updateUserPassword(
            @Nullable final String userId,
            @Nullable final String password,
            @Nullable final String old
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        if (old == null || old.isEmpty()) throw new AccessDeniedException("Wrong old password. Try again.");
        @Nullable final User user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        final boolean isOldPassEquals = user.getPassHash().equals(mdHashCode(old));
        if (!isOldPassEquals) throw new AccessDeniedException("Old password did not match.");
        user.setPassHash(mdHashCode(password));
        merge(user);
    }

    @Override
    public void loadFromDto(@Nullable final List<User> userList) throws Exception {
        if (userList == null) return;
        for (final User user : userList){
            @Nullable User u = findOne(user.getId());
            if (u == null) persist(user);
            else merge(user);
        }
    }

    @NotNull
    @Override
    public Role getUserRole(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        @Nullable final User user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        @Nullable final Role userRole = user.getRole();
        return userRole;
    }

}