package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.mapper.SessionMapper;
import ru.rastorguev.tm.api.service.ISessionFactoryService;
import ru.rastorguev.tm.api.service.ISessionService;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionFactoryService sessionFactoryService;

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        @NotNull final List<Session> sessionList = mapper.findAll();
        sqlSession.close();
        return sessionList;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String sessionId) throws Exception {
        if (sessionId == null || sessionId.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        @Nullable final Session session = mapper.findOne(sessionId);
        sqlSession.close();
        return session;
    }

    @Nullable
    @Override
    public Session persist(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        try{
            mapper.persist(session);
            sqlSession.commit();
            return session;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session merge(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        try{
            mapper.merge(session);
            sqlSession.commit();
            return session;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String sessionId) throws Exception {
        if (sessionId == null || sessionId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        try{
            mapper.remove(sessionId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean isContains(@NotNull final Session session) throws Exception {
        @Nullable final Session checkThisSession = findOne(session.getId());
        if (checkThisSession == null) return false;

        final boolean isUserIdEquals = Objects.equals(checkThisSession.getUserId(), session.getUserId());
        final boolean isCreateDateEquals = Objects.equals(checkThisSession.getTimestamp(), session.getTimestamp());
        final boolean isSignatureEquals = Objects.equals(checkThisSession.getSignature(), session.getSignature());
        final boolean isRoleEquals = Objects.equals(checkThisSession.getRole(), session.getRole());

        return isUserIdEquals && isCreateDateEquals && isSignatureEquals && isRoleEquals;
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied.");
        @Nullable final String userId = session.getUserId();
        @Nullable final String signature = session.getSignature();
        @Nullable final Role role = session.getRole();
        if (userId == null || userId.isEmpty() || signature == null || signature.isEmpty() || role == null) {
            throw new AccessDeniedException("Access denied.");
        }

        if (!isContains(session)) throw new AccessDeniedException("Access denied.");

        final long timeOfExistence = System.currentTimeMillis() - session.getTimestamp();
        if (timeOfExistence > Constant.SESSION_LIFETIME) {
            remove(session.getId());
            throw new AccessDeniedException("Session is over.");
        }
    }

    public void validateSessionAndRole(@Nullable final Session session, @NotNull final Role role) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied.");
        validate(session);
        if (!role.equals(session.getRole())) throw new AccessDeniedException("Access denied. Role type is not allowed.");
    }

}
