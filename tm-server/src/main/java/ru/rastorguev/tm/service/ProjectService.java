package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.mapper.ProjectMapper;
import ru.rastorguev.tm.api.service.IProjectService;
import ru.rastorguev.tm.api.service.ISessionFactoryService;
import ru.rastorguev.tm.entity.Project;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final ISessionFactoryService sessionFactoryService;


    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> projectList = mapper.findAll();
        sqlSession.close();
        return projectList;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @Nullable final Project project = mapper.findOne(projectId);
        sqlSession.close();
        return project;
    }

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) throws Exception {
        if (project == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.persist(project);
            sqlSession.commit();
            return project;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project project) throws Exception {
        if (project == null) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.merge(project);
            sqlSession.commit();
            return project;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.remove(projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> projectList = mapper.findAllByUserId(userId);
        sqlSession.close();
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> projectList = mapper.findAllByUserIdSorted(userId, sortType);
        sqlSession.close();
        return projectList;
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public String getProjectIdByNumberForUser(final int number, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final List<Project> filteredListOfProjects = findAllByUserId(userId);
        return filteredListOfProjects.get(number - 1).getId();
    }

    @NotNull
    @Override
    public List<Project> findProjectsByInputAndUserId(@NotNull final String input, @NotNull final String userId) throws Exception {
        if (userId.isEmpty() || input.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> projectList = mapper.findProjectsByInputAndUserId(input, userId);
        sqlSession.close();
        return projectList;
    }

    @Override
    public void loadFromDto(@Nullable final List<Project> projectList) throws Exception {
        if (projectList == null) return;
        for (final Project project : projectList){
            @Nullable Project p = findOne(project.getId());
            if (p == null) persist(project);
            else merge(project);
        }
    }

}