package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.IService;
import ru.rastorguev.tm.entity.AbstractEntity;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    @Override
    public abstract List<E> findAll() throws Exception;

    @Nullable
    @Override
    public abstract E findOne(@Nullable final String entityId) throws Exception;

    @Nullable
    @Override
    public abstract E persist(@Nullable final E entity) throws Exception;

    @Nullable
    @Override
    public abstract E merge(@Nullable final E entity) throws Exception;

    @Override
    public abstract void remove(@Nullable final String entityId) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

}