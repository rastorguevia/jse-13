package ru.rastorguev.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ISessionFactoryService;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.util.PropertyUtil;
import ru.rastorguev.tm.api.mapper.*;

import javax.sql.DataSource;

public final class SessionFactoryService implements ISessionFactoryService {

    @NotNull
    public SqlSessionFactory getSqlSessionFactory() throws Exception {
        @Nullable final String user = PropertyUtil.getDbProperty(Constant.DB_USER);
        @Nullable final String password = PropertyUtil.getDbProperty(Constant.DB_PASSWORD);
        @Nullable final String url = PropertyUtil.getDbProperty(Constant.DB_URL);
        @Nullable final String driver = PropertyUtil.getDbProperty(Constant.DB_DRIVER);
        final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment(Constant.MYBATIS_ENVIRONMENT_ID, transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(UserMapper.class);
        configuration.addMapper(ProjectMapper.class);
        configuration.addMapper(TaskMapper.class);
        configuration.addMapper(SessionMapper.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    public SqlSession getSession() throws Exception {
        return getSqlSessionFactory().openSession();
    }

}
