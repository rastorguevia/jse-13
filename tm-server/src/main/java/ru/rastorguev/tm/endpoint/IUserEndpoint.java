package ru.rastorguev.tm.endpoint;

import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint {

    @WebMethod
    User findCurrentUser(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    User createUser(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) throws Exception;

    @WebMethod
    User createAdmin(
            @WebParam(name = "token") final String token,
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) throws Exception;

    @WebMethod
    void updateUserLogin(
            @WebParam(name = "token") final String token,
            @WebParam(name = "login") final String login
    ) throws Exception;

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "token") final String token,
            @WebParam(name = "password") final String password,
            @WebParam(name = "oldPassword") final String old
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "token") final String token
    ) throws Exception;

    @WebMethod
    boolean isExistByLogin(
            @WebParam(name = "token") final String token,
            @WebParam(name = "login") final String login
    ) throws Exception;

    Role getUserRole(
            @WebParam(name = "token") final String token
    ) throws Exception;

    String getUserId(
            @WebParam(name = "token") final String token
    ) throws Exception;

    void removeUser(
            @WebParam(name = "token") final String token
    ) throws Exception;

}
