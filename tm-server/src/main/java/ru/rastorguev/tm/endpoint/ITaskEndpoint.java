package ru.rastorguev.tm.endpoint;

import ru.rastorguev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    Task createTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "task") final Task task
    ) throws Exception;

    @WebMethod
    Task updateTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "task") final Task task
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "taskId") final String taskId
    ) throws Exception;

    @WebMethod
    void removeTaskListByProjectId(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") final String projectId
    ) throws Exception;

    @WebMethod
    String taskIdByNumberAndProjectId(
            @WebParam(name = "token") final String token,
            @WebParam(name = "number") final int number,
            @WebParam(name = "projectId") final String projectId
    ) throws Exception;

    @WebMethod
    List<Task> taskListByUserId(
            @WebParam(name = "token") final String token,
            @WebParam(name = "userId") final String userId
    ) throws Exception;

    @WebMethod
    List<Task> filteredTaskListByInput(
            @WebParam(name = "token") final String token,
            @WebParam(name = "input") final String input
    ) throws Exception;

    @WebMethod
    List<Task> getTaskListByProjectId(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") final String projectId
    ) throws Exception;

    @WebMethod
    List<Task> getTaskListByProjectIdSorted(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") final String projectId,
            @WebParam(name = "sortType") final String sortType
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "token") final String token
    ) throws Exception;

    @WebMethod
    Task findTask (
            @WebParam(name = "token") final String token,
            @WebParam(name = "taskId") final String taskId
    ) throws Exception;

}
