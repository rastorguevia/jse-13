package ru.rastorguev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDataEndpoint {

    @WebMethod
    void dataSaveBinary(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataLoadBinary(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataSaveJaxbXml(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataLoadJaxbXml(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataSaveJaxbJson(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataLoadJaxbJson(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataSaveFasterXmlXml(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataLoadFasterXmlXml(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataSaveFasterXmlJson(@WebParam(name = "token") final String token) throws Exception;

    @WebMethod
    void dataLoadFasterXmlJson(@WebParam(name = "token") final String token) throws Exception;

}
