package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    Project createProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "project") final Project project
    ) throws Exception;

    @WebMethod
    Project updateProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "project") final Project project
    ) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "project") final String projectId
    ) throws Exception;

    @WebMethod
    void removeAllProjectsByUser(
            @WebParam(name = "token") final String token
    ) throws Exception;

    @WebMethod
    List<Project> findAllProjectsForUser(
            @WebParam(name = "token") final String token
    ) throws Exception;

    @WebMethod
    List<Project> findAllProjectsForUserSorted(
            @WebParam(name = "token") final String token,
            @WebParam(name = "sortType") final String sortType
    ) throws Exception;

    @WebMethod
    List<Project> findProjectsByUserIdAndInput(
            @WebParam(name = "token") final String token,
            @WebParam(name = "input") final String input
    ) throws Exception;

    @WebMethod
    String projectIdByNumber(
            @WebParam(name = "token") final String token,
            @WebParam(name = "number") final int number
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "token") final String token
    ) throws Exception;

    @WebMethod
    Project findProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") final String projectId
    ) throws Exception;

}
